﻿/// <reference path="jquery-2.1.4.js" />
var ajaxenabled = false;
document.addEventListener("DOMContentLoaded", initfunc);

function initfunc() {
    console.log("Book Manager script inizilized.");
    $("#jsctrls").show();
    $('#hidenav').hide();
    if (XMLHttpRequest) {
        console.log("AJAX available.");
        ajaxenabled = true;
        $.get("/Home/AJAXAllBookList", null, drawlist, "json");
        $("#searchbox").on("keyup", sbhandler);
        $.get("/Home/AJAXGetCats", null, drawcats, "json");
        $("#catselect").on("change", sbhandler);
    } else {
        var mb = $("#mainblock");
        mb.detach;
        mb.appendTo("body");
    }
}

function drawlist(list) {
    console.log("Attempting to draw new book list with", list.length, "elements.");
    var tblbody = $("#booktable tbody").first();
    tblbody.empty();
    if (list.length > 0) {
        for (var i = 0; i < list.length; i++) {
            tblbody.append(
                    "<tr>" +
                    "<td>" + list[i].ISBN + "</td>" +
                    "<td>" + list[i].Titel + "</td>" +
                    "<td>" + list[i].Erschienen + "</td>" +
                    "<td>" + list[i].Kategorie + "</td>" +
                    "</tr>"
                );
        }
    }
}

function sbhandler() {
    console.log("Inside sbhandler");
    
    $.post("/Home/AJAXSearchBook", {
        "searchterm": $("#searchbox").val(),
        "catid": $("#catselect").val()
    }, drawlist, "json");
    
}

function drawcats(cats) {
    var catselect = $("#catselect");
    catselect.empty();
    catselect.append("<option id='default' selected='selected' value='-1'> -- Alle --</option>");
    for (var i = 0; i < cats.length; i++) {
        catselect.append("<option id='" + cats[i].CategoryID + "' value='" + cats[i].CategoryID + "'>" + cats[i].CategoryName + "</option>")
    }
}