﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebBuecherei.Models;

namespace WebBuecherei.Controllers
{
    public class HomeController : Controller
    {

        BooksDBEntities ctx = new BooksDBEntities();

        public ActionResult Index()
        {
            //Test
            return View();
        }

        //Action: Alle Bücher anziegen - unvollständig
        //ActionResult geschreiben, View hinzugefügt
        public ActionResult ShowAllBooks()
        {
            return View(ctx);
        }

        //Action: Bücher suchen - unvollständig
        //ActionResult geschreiben, View hinzugefügt
        public ActionResult FindBooks(string searchterm)
        {
            if (searchterm == null || searchterm == "")
            {
                return View(new List<Book>());
            }
            else
        {
                List<Book> results = new List<Book>();
                results = ctx.Books.Where(b => b.Title.Contains(searchterm)).ToList();

                return View(results);
            }
        }

        //Action: Bücher nach Kategorie anzeigen lassen - unvollständig
        //ActionResult geschreiben, View hinzugefügt
        public ActionResult BooksByCategory(int? CatID)
        {
            var cats = ctx.Categories.Distinct().ToList();

            if (CatID != null)
            {
                var result = ctx.Books.Where(b => b.CategoryID == CatID).ToList();
                return View(new Tuple<List<Book>, List<Category>>(result, cats));
            }
            else
            {
                return View(new Tuple<List<Book>, List<Category>>(new List<Book>(), cats));
            }
        }     

        public JsonResult AJAXAllBookList()
        {
            
            var allbooks = ctx.Books.Select(b => new { ISBN = b.ISBN, Titel = b.Title, Erschienen = b.Year.ToString(), Kategorie = b.Category.CategoryName });


            return Json(allbooks, JsonRequestBehavior.AllowGet);
                        
        }

        public JsonResult AJAXSearchBook(string searchterm, int? catid)
        {
            IQueryable<dynamic> foundbooks = null;
            if (searchterm != null && searchterm != "")
            {
                

                if (catid == null || catid < 1)
                {
                    foundbooks = ctx.Books.Where(x => x.Title.Contains(searchterm)).Select(b => new { ISBN = b.ISBN, Titel = b.Title, Erschienen = b.Year.ToString(), Kategorie = b.Category.CategoryName });
                }
                else
                {
                    foundbooks = ctx.Books.Where(x => x.CategoryID == catid && x.Title.Contains(searchterm)).Select(b => new { ISBN = b.ISBN, Titel = b.Title, Erschienen = b.Year.ToString(), Kategorie = b.Category.CategoryName });
                }

                return Json(foundbooks, JsonRequestBehavior.AllowGet);
            }
            else 
            {
                if (catid == null || catid < 1)
                {
                    foundbooks = ctx.Books.Select(b => new { ISBN = b.ISBN, Titel = b.Title, Erschienen = b.Year.ToString(), Kategorie = b.Category.CategoryName });
                }
                else
                {
                    foundbooks = ctx.Books.Where(x => x.CategoryID == catid).Select(b => new { ISBN = b.ISBN, Titel = b.Title, Erschienen = b.Year.ToString(), Kategorie = b.Category.CategoryName });
                }
                return Json(foundbooks, JsonRequestBehavior.AllowGet);

            }
        }

        public JsonResult AJAXGetCats()
        {
            var results = ctx.Categories.Select(c => new { CategoryID = c.CategoryID, CategoryName = c.CategoryName });
            return Json(results, JsonRequestBehavior.AllowGet);
        }
      
    }
}